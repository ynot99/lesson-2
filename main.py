def get_denominator() -> int:
    try:
        with open("denom.txt", "r") as file:
            denominator: int = int(file.read())
            if denominator == 0:
                raise ZeroDivisionError
            return denominator
    except FileNotFoundError:
        print("denom.txt was not found")
    except ValueError:
        print("not a number was given")
    except ZeroDivisionError:
        print("zero was given")

    print(f"{get_denominator.__name__} should return int")
    exit(1)


def get_list_of_numbers(denominator) -> list:
    return filter(lambda num: num % denominator == 0, range(1, 101))


def get_sum(list_of_numbers) -> int:
    return sum(list_of_numbers)


def write_result(number) -> None:
    with open("result.txt", "w") as file:
        file.write(str(number))


denominator = get_denominator()
list_of_numbers = get_list_of_numbers(denominator)
sum_of_numbers = get_sum(list_of_numbers)
write_result(sum_of_numbers)
