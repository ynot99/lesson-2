from time import time


class Timer:
    def __init__(self, filename):
        self.file = open(filename, "a")

    def __enter__(self):
        self.time_start = time()

    def __exit__(self, exc_type, exc_val, exc_tb):
        done_by_in_milliseconds = int((time() - self.time_start) * 1000)
        status_text = "Success" if not exc_type else exc_val
        self.file.write(f"Done by {done_by_in_milliseconds} milliseconds. Status: {status_text}\n")
        self.file.close()
        return True


def numbers(start, end) -> int:
    if (type(start) is not int) or (type(end) is not int):
        raise TypeError("Error, start and end must be integer values")
    if start >= end:
        raise ValueError("Error, start must be less than end")

    i = start
    while i < end:
        yield i
        i += 1


with Timer("file.txt"):
    for i in numbers(10, 20):
        print(i)


with Timer("file.txt"):
    for i in numbers(10, 10):
        print(i)


with Timer("file.txt"):
    for i in numbers("asd", "asd"):
        print(i)
